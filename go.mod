module gitlab.com/hachathon-covid2020/ladoescurodalua

go 1.14

require (
	github.com/IBM/ibm-cos-sdk-go v1.3.2
	github.com/aws/aws-sdk-go v1.33.12
	github.com/cloudfoundry-community/go-cfenv v1.18.0
	github.com/dgraph-io/ristretto v0.0.3
	github.com/google/uuid v1.1.1
	github.com/labstack/echo/v4 v4.1.16
	github.com/rs/zerolog v1.19.0
	github.com/timjacobi/go-couchdb v0.0.0-20160817113035-5f9d2a1a29e5
	gitlab.com/felipemocruha/gonfig v0.2.0
)
