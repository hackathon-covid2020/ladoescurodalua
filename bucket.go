package main

import (
	"bytes"
	"os"

	"github.com/IBM/ibm-cos-sdk-go/aws"
	"github.com/IBM/ibm-cos-sdk-go/aws/credentials/ibmiam"
	"github.com/IBM/ibm-cos-sdk-go/aws/session"
	"github.com/IBM/ibm-cos-sdk-go/service/s3"
	"github.com/rs/zerolog/log"
)

const (
	STORAGE_AUTH     = "https://iam.cloud.ibm.com/identity/token"
	STORAGE_ENDPOINT = "s3.us-south.cloud-object-storage.appdomain.cloud"
)

type Bucket struct {
	client *s3.S3
}

func NewBucketClient() *Bucket {
	conf := aws.NewConfig().
		WithEndpoint(STORAGE_ENDPOINT).
		WithCredentials(ibmiam.NewStaticCredentials(aws.NewConfig(),
			STORAGE_AUTH, os.Getenv("STORAGE_API_KEY"), os.Getenv("STORAGE_INSTANCE_ID"))).
		WithS3ForcePathStyle(true)
	sess, err := session.NewSession()
	if err != nil {
		log.Fatal().Msgf("failed to create bucket client: %v", err)
	}

	return &Bucket{s3.New(sess, conf)}
}

func (b Bucket) Upload(file []byte, key, content string) error {
	_, err := b.client.PutObject(&s3.PutObjectInput{
		Bucket:      aws.String("museums-cloud-images"),
		Key:         aws.String(key),
		ACL:         aws.String("public-read"),
		Body:        bytes.NewReader(file),
		ContentType: aws.String(content),
	})
	if err != nil {
		return err
	}

	return nil
}
