package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	//	"github.com/cloudfoundry-community/go-cfenv"
	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
)

func main() {
	service := NewService()
	service.Start()
}

type Service struct {
	repository *Repository
	bucket     *Bucket
}

func NewService() *Service {
	// _, err := cfenv.Current()
	// if err != nil {
	// 	log.Fatal().Msgf("failed to get current app context: %v", err)
	// }
	sess, err := session.NewSession()
	if err != nil {
		log.Fatal().Msgf("failed to create db session: %v", err)
	}
	repo := &Repository{dynamodb.New(sess), NewCache()}

	return &Service{repo, NewBucketClient()}
}

func (s Service) Start() {
	server := echo.New()
	server.HideBanner = true
	server.HidePort = true
	server.Server.ReadTimeout = 10 * time.Second
	server.Server.WriteTimeout = 10 * time.Second
	server.Server.IdleTimeout = 120 * time.Second

	// server.Use(middleware.CORSWithConfig(middleware.CORSConfig{
	// 	AllowOrigins: []string{
	// 		"https://registration.museums.cloud",
	// 		"https://museums.cloud",
	// 	},
	// }))
	server.Use(middleware.CORS())

	server.GET("/api/healthcheck", func(ctx echo.Context) error {
		return ctx.JSON(200, map[string]string{"message": "olar"})
	})

	server.POST("/api/museums", s.CreateMuseum)
	//	server.GET("/api/museums", s.ListMuseums) vai ter?

	server.POST("/api/upload", s.UploadImage)
	server.POST("/api/collections", s.CreateCollection)
	server.GET("/api/collections/:museumId", s.GetMuseumCollection)

	server.POST("/api/exhibitions", s.CreateExhibition)
	server.GET("/api/exhibitions", s.ListExhibitions)
	server.GET("/api/exhibitions/:id", s.GetExhibitionDetails)

	host := fmt.Sprintf("0.0.0.0:%v", os.Getenv("VCAP_APP_PORT"))
	log.Info().Msgf("starting server at: %v", host)
	server.Start(host)
}

func (s Service) GetMuseumCollection(ctx echo.Context) error {
	resp := &GetMuseumCollectionResponse{}

	res, err := s.repository.GetMuseumCollection(ctx.Param("museumId"))
	if err != nil {
		log.Error().Msgf("failed to get exhibition details: %v", err)
		return err
	}
	resp.Collection = res

	return ctx.JSON(200, resp)
}

func (s Service) CreateMuseum(ctx echo.Context) error {
	museum := &CreateMuseumRequest{}
	if err := ctx.Bind(museum); err != nil {
		log.Error().Msgf("failed to deserialize request body: %v", err)
		return echo.NewHTTPError(400, err)
	}
	resp := museum.Validate()

	if err := s.repository.CreateMuseum(museum); err != nil {
		log.Error().Msgf("failed to create museum: %v", err)
		return echo.NewHTTPError(500, err)
	}

	return ctx.JSON(201, resp)
}

func (s Service) CreateCollection(ctx echo.Context) error {
	collection := &CreateCollectionRequest{
		CreatedAtUnix: time.Now().Unix(),
	}
	if err := ctx.Bind(collection); err != nil {
		log.Error().Msgf("failed to deserialize request body: %v", err)
		return echo.NewHTTPError(400, err)
	}

	if err := s.repository.CreateCollection(collection); err != nil {
		log.Error().Msgf("failed to create collection: %v", err)
		return echo.NewHTTPError(500, err)
	}

	return ctx.JSON(201, "Created")
}

func (s Service) CreateExhibition(ctx echo.Context) error {
	exhibition := &CreateExhibitionRequest{}
	if err := ctx.Bind(exhibition); err != nil {
		log.Error().Msgf("failed to deserialize request body: %v", err)
		return echo.NewHTTPError(400, err)
	}
	resp := exhibition.Validate()

	if err := s.repository.CreateExhibition(exhibition); err != nil {
		log.Error().Msgf("failed to create exhibition: %v", err)
		return echo.NewHTTPError(500, err)
	}

	return ctx.JSON(201, resp)
}

const BUCKET_PUBLIC_URL = "https://museums-cloud-images.s3.us-south.cloud-object-storage.appdomain.cloud"

func (s Service) UploadImage(ctx echo.Context) error {
	ctx.Request().ParseMultipartForm(10 << 20)
	file, _, err := ctx.Request().FormFile("collection-photo")
	if err != nil {
		log.Error().Msgf("failed to read form file: %v", err)
		return err
	}
	defer file.Close()

	buf, err := ioutil.ReadAll(file)
	if err != nil {
		log.Error().Msgf("failed to read data into buffer: %v", err)
		return err
	}

	id := uuid.New().String()
	content := http.DetectContentType(buf)

	extension := "jpeg"
	switch content {
	case "image/png":
		extension = "png"
	}

	key := fmt.Sprintf("%v.%v", id, extension)
	if err := s.bucket.Upload(buf, key, content); err != nil {
		log.Error().Msgf("failed to upload file: %v", err)
		return err
	}
	imageURL := fmt.Sprintf("%v/%v", BUCKET_PUBLIC_URL, key)

	return ctx.JSON(201, map[string]string{"imageUrl": imageURL})
}

func (s Service) ListExhibitions(ctx echo.Context) error {
	resp := &ListExhibitionsResponse{}
	res, err := s.repository.ListExhibitions()
	if err != nil {
		log.Error().Msgf("failed to list exhibitions: %v", err)
		return err
	}
	resp.Exhibitions = res

	return ctx.JSON(200, resp)
}

func (s Service) GetExhibitionDetails(ctx echo.Context) error {
	resp := &GetExhibitionDetailsResponse{}

	res, err := s.repository.GetExhibitionDetails(ctx.Param("id"))
	if err != nil {
		log.Error().Msgf("failed to get exhibition details: %v", err)
		return err
	}
	resp.Details = res

	return ctx.JSON(200, resp)
}
