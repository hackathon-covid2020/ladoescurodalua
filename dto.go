package main

import (
	"time"

	"github.com/google/uuid"
)

type CreateMuseumRequest struct {
	Id            string `json:"id"`
	CreatedAtUnix int64  `json:"created_at_unix"`

	Owner               string `json:"owner"`
	Name                string `json:"name"`
	Kind                string `json:"kind"`
	Address             string `json:"address"`
	ShortDescription    string `json:"shortDescription"`
	WebsiteURL          string `json:"website"`
	ContactEmail        string `json:"contact_email"`
	DateOfEstablishment string `json:"date_of_establishment"`
	Bio                 string `json:"bio"`
}

type CreateMuseumResponse struct {
	Id            string `json:"id"`
	CreatedAtUnix int64  `json:"created_at_unix"`
}

func (r *CreateMuseumRequest) Validate() *CreateMuseumResponse {
	r.Id = uuid.New().String()
	r.CreatedAtUnix = time.Now().Unix()

	return &CreateMuseumResponse{r.Id, r.CreatedAtUnix}
}

type CollectionItem struct {
	Title           string `json:"title"`
	Author          string `json:"author"`
	Kind            string `json:"kind"`
	Country         string `json:"country"`
	ProductionDate  string `json:"productionDate"`
	AcquisitionDate string `json:"acquisitionDate"`
	Image           string `json:"imageUrl"`
}

type CreateCollectionRequest struct {
	MuseumId      string           `json:"museumId"`
	MuseumName    string           `json:"museumName"`
	Items         []CollectionItem `json:"items"`
	CreatedAtUnix int64            `json:"created_at_unix"`
}

type MuseumCollection struct {
	MuseumId      string           `json:"museumId"`
	MuseumName    string           `json:"museumName"`
	Items         []CollectionItem `json:"items"`
	CreatedAtUnix int64            `json:"created_at_unix"`
}

type GetMuseumCollectionResponse struct {
	Collection *MuseumCollection `json:"collection"`
}

type CatalogueItem struct {
	Title           string `json:"title"`
	Author          string `json:"author"`
	Kind            string `json:"kind"`
	Country         string `json:"country"`
	ImageUrl        string `json:"imageUrl"`
	ProductionYear  string `json:"productionYear"`
	InformationBit  string `json:"informationBit"`
	BackgroundColor string `json:"backgroundColor"`
	Theme           string `json:"theme"`
	BackgroundImage string `json:"backgroundImage"`
}

type CreateExhibitionRequest struct {
	Id            string `json:"id"`
	CreatedAtUnix int64  `json:"created_at_unix"`

	MuseumId      string          `json:"museumId"`
	MuseumName    string          `json:"museumName"`
	Title         string          `json:"title"`
	StartDate     string          `json:"startDate"`
	EndDate       string          `json:"endDate"`
	Introduction  string          `json:"introduction"`
	Conclusion    string          `json:"conclusion"`
	CoverImageURL string          `json:"coverImageUrl"`
	Catalogue     []CatalogueItem `json:"catalogue"`
}

type CreateExhibitionResponse struct {
	Id            string `json:"id"`
	CreatedAtUnix int64  `json:"created_at_unix"`
}

func (r *CreateExhibitionRequest) Validate() *CreateExhibitionResponse {
	r.Id = uuid.New().String()
	r.CreatedAtUnix = time.Now().Unix()

	return &CreateExhibitionResponse{r.Id, r.CreatedAtUnix}
}

type ExhibitionListItem struct {
	Id            string `json:"id"`
	MuseumName    string `json:"museumName"`
	Title         string `json:"title"`
	StartDate     string `json:"startDate"`
	EndDate       string `json:"endDate"`
	Introduction  string `json:"introduction"`
	CoverImageUrl string `json:"coverImageUrl"`
}

type ListExhibitionsResponse struct {
	Exhibitions []ExhibitionListItem `json:"exhibitions"`
}

type ExhibitionDetails struct {
	MuseumId     string          `json:"museumId"`
	MuseumName   string          `json:"museumName"`
	Title        string          `json:"title"`
	StartDate    string          `json:"startDate"`
	EndDate      string          `json:"endDate"`
	Introduction string          `json:"introduction"`
	Conclusion   string          `json:"conclusion"`
	DonationUrl  string          `json:"donationUrl"`
	Catalogue    []CatalogueItem `json:"catalogue"`
}

type GetExhibitionDetailsResponse struct {
	Details *ExhibitionDetails `json:"details"`
}
