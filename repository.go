package main

import (
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/dgraph-io/ristretto"
	"github.com/rs/zerolog/log"
)

const MUSEUMS_TABLE = "museums"
const COLLECTIONS_TABLE = "collections"
const EXHIBITIONS_TABLE = "exhibitions"

func NewCache() *ristretto.Cache {
	cache, err := ristretto.NewCache(&ristretto.Config{
		NumCounters: 1e7,
		MaxCost:     100_000_000,
		BufferItems: 64,
	})
	if err != nil {
		log.Fatal().Msgf("failed to create cache: %v", err)
	}

	return cache
}

type Repository struct {
	db    *dynamodb.DynamoDB
	cache *ristretto.Cache
}

func (r Repository) CreateMuseum(req *CreateMuseumRequest) error {
	av, err := dynamodbattribute.MarshalMap(req)
	if err != nil {
		return err
	}

	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(MUSEUMS_TABLE),
	}

	_, err = r.db.PutItem(input)
	return err
}

func (r Repository) CreateCollection(req *CreateCollectionRequest) error {
	av, err := dynamodbattribute.MarshalMap(req)
	if err != nil {
		return err
	}

	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(COLLECTIONS_TABLE),
	}

	_, err = r.db.PutItem(input)
	return err
}

func (r Repository) CreateExhibition(req *CreateExhibitionRequest) error {
	av, err := dynamodbattribute.MarshalMap(req)
	if err != nil {
		return err
	}

	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(EXHIBITIONS_TABLE),
	}

	_, err = r.db.PutItem(input)
	return err
}

func (r Repository) ListExhibitions() ([]ExhibitionListItem, error) {
	key := "LIST_EXHIBITIONS"

	res, cached := r.cache.Get(key)
	if !cached {
		proj := expression.NamesList(
			expression.Name("id"),
			expression.Name("museumName"),
			expression.Name("title"),
			expression.Name("startDate"),
			expression.Name("endDate"),
			expression.Name("introduction"),
			expression.Name("catalogue"),
		)
		expr, err := expression.NewBuilder().WithProjection(proj).Build()
		if err != nil {
			return nil, err
		}

		params := &dynamodb.ScanInput{
			ExpressionAttributeNames:  expr.Names(),
			ExpressionAttributeValues: expr.Values(),
			ProjectionExpression:      expr.Projection(),
			TableName:                 aws.String(EXHIBITIONS_TABLE),
		}

		result, err := r.db.Scan(params)
		if err != nil {
			return nil, err
		}

		out := []ExhibitionListItem{}
		err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &out)
		if err != nil {
			return nil, err
		}

		r.cache.SetWithTTL(key, out, 1, 12*time.Hour)

		return out, nil
	}

	return res.([]ExhibitionListItem), nil
}

func (r Repository) GetExhibitionDetails(id string) (*ExhibitionDetails, error) {
	details := &ExhibitionDetails{}
	key := "GET_EXHIBITION_DETAILS"

	res, cached := r.cache.Get(key)
	if !cached {
		result, err := r.db.GetItem(&dynamodb.GetItemInput{
			TableName: aws.String(EXHIBITIONS_TABLE),
			Key: map[string]*dynamodb.AttributeValue{
				"id": {S: aws.String(id)},
			},
		})
		if err != nil {
			return nil, err
		}

		err = dynamodbattribute.UnmarshalMap(result.Item, details)
		if err != nil {
			return nil, err
		}
		r.cache.Set(key, details, 1)

		return details, nil
	}

	return res.(*ExhibitionDetails), nil
}

func (r Repository) GetMuseumCollection(id string) (*MuseumCollection, error) {
	coll := &MuseumCollection{}
	key := "GET_MUSEUM_COLLECTION"

	res, cached := r.cache.Get(key)
	if !cached {
		result, err := r.db.GetItem(&dynamodb.GetItemInput{
			TableName: aws.String(COLLECTIONS_TABLE),
			Key: map[string]*dynamodb.AttributeValue{
				"museumId": {S: aws.String(id)},
			},
		})
		if err != nil {
			return nil, err
		}

		err = dynamodbattribute.UnmarshalMap(result.Item, coll)
		if err != nil {
			return nil, err
		}
		r.cache.Set(key, coll, 1)

		return coll, nil
	}

	return res.(*MuseumCollection), nil
}
